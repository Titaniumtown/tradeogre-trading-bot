import httpx as requests

class WebhookManager:
    def __init__(self, webhook_url):
        if webhook_url == "":
            raise Exception("webhook_url must be valid")
        
        self.webhook_url = webhook_url

    def send_webhook(self, msg):
        data = {
            "content": str(msg)
        }

        requests.post(self.webhook_url, data=data)